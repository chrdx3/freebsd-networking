##**Bridges**

`ifconfig bridge destroy` to destroy pseudo-device bridge0

`ifconfig bridge0 addm em0 addm em1 addm em2 up`'  to add interfaces in bridge and enable bridge.

Note: The interfaces must be also up.

`ifconfig bridge0 deletem em0 `  to delete interface from bridge 

`ifconfig bridge0 addr` to display the forwarding table, addresses and the respective port

`ifconfig bridge0 flush`  to flush the forwarding table, deleting the addresses obtained dynamically by the bridge

`ifconfig bridge0 stp em0 stp em1 stp em2`  to enable STP protocol on the respective interfaces

`ifconfig bridge0 -stp em0 -stp em1 -stp em2` to disable STP protocol on the respective interfaces

`ifconfig bridge priority value1`  to set the priority of bridge0. The default is 32768, min:0 - max: 61440

`ifconfig bridge0 ifpriority em0 value` to set the priority of em0 interface of bridge0

`ifconfig bridge0 ifpathcost em0 value` to set the cost of em0 of bridge0
